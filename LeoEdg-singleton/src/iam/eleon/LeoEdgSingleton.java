package iam.eleon;

import java.util.Scanner;

public class LeoEdgSingleton {

	private static  Scanner instance = null;
		  
	public static Scanner getInstance() {
		if(instance == null) {
			instance = new Scanner(System.in);
		}
		return instance;
	}
	
	public static void main(String[] args) {
		
		String nom;
		
		instance = LeoEdgSingleton.getInstance();
		
		System.out.println("Introdueix el teu nom:");
		
		nom=instance.nextLine();
		
		System.out.println(nom);
	}
}
